package enums;

public enum JsonFields {
CUSTOMER_ID("customerId"),TAPS("taps"),
TIME("unixTimestamp"),STATION("station"),
CUSTOMER_SUMMARY("customerSummaries"),
	TOTAL_COST_IN_CENTS("totalCostInCents"),
	TRIPS("trips"),START_STATION("stationStart"),
	END_STATION("stationEnd"),JOURNEY_START_TIME("startedJourneyAt"),
	COST_IN_CENTS("costInCents"),START_ZONE("zoneFrom"),
	END_ZONE("zoneTo");
	
	
	
	private String property;
	private JsonFields(String property) {
		this.property=property;
	}
	
	public String getProperty() {
		return this.property;
	}
}
