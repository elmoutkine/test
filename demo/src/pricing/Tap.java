package pricing;

public class Tap  {
	
	private Long unitTimeStamp;
	private Long customerId;
	private String station;
	public Long getUnitTimeStamp() {
		return unitTimeStamp;
	}
	public void setUnitTimeStamp(Long unitTimeStamp) {
		this.unitTimeStamp = unitTimeStamp;
	}
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}

	

}
