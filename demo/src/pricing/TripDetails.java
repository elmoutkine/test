package pricing;

//contient les variables qui seront en output
public class TripDetails {
	private Long customerId;
	private String stationStart;
	private String stationEnd;
	private Long journeyStart;
	private Integer cost;
	private Integer startZone;
	private Integer endZone;
	
	public Long getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}
	public String getStationStart() {
		return stationStart;
	}
	public void setStationStart(String stationStart) {
		this.stationStart = stationStart;
	}
	public String getStationEnd() {
		return stationEnd;
	}
	public void setStationEnd(String stationEnd) {
		this.stationEnd = stationEnd;
	}
	public Long getJourneyStart() {
		return journeyStart;
	}
	public void setJourneyStart(Long journeyStart) {
		this.journeyStart = journeyStart;
	}
	public Integer getCost() {
		return cost;
	}
	public void setCost(Integer cost) {
		this.cost = cost;
	}
	public Integer getStartZone() {
		return startZone;
	}
	public void setStartZone(Integer startZone) {
		this.startZone = startZone;
	}
	public Integer getEndZone() {
		return endZone;
	}
	public void setEndZone(Integer endZone) {
		this.endZone = endZone;
	}
	

}
