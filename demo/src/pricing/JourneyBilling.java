package pricing;


import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import enums.JsonFields;

public class JourneyBilling {
	
	public static void main(String[] args) {
		JourneyBilling billing = new JourneyBilling();
		billing.manageJourneyBilling(args[0], args[1]);
		//billing.manageJourneyBilling("C:/Users/pc/Desktop/exmpleDemoTrain/input.txt", "C:/Users/pc/Desktop/exmpleDemoTrain/output.txt");
	}
	
	public void manageJourneyBilling(String inputFilePath ,
			String outputFilePath) {
		JSONParser jsonParser= new JSONParser();
		
		try {
			Object obj= jsonParser.parse(new FileReader(inputFilePath));
			JSONObject jsonInput=(JSONObject) obj;
			JSONArray taps= (JSONArray) jsonInput.get(JsonFields.TAPS.getProperty());
			ArrayList tapsList= new ArrayList<Tap>();
			taps.forEach((jsonTap)->{
				Tap tap = extractTap((JSONObject)jsonTap);
				tapsList.add(tap);
			});
			Comparator<Tap> compareByCustomer = (Tap a,Tap b) ->{ Integer comparator=0;
												comparator = a.getCustomerId().compareTo(b.getCustomerId());
												return comparator;};
			Comparator<Tap> compareByTime=(Tap a,Tap b) ->{
				Integer comparator = 0;
				if(a.getCustomerId().compareTo(b.getCustomerId())==0) {
					return a.getUnitTimeStamp().compareTo(b.getUnitTimeStamp());
				}
				return comparator;
			};
			Collections.sort(tapsList,compareByCustomer);
			Collections.sort(tapsList,compareByTime);
			ArrayList<TripDetails> details = extractTripDetails(tapsList);
			Comparator<TripDetails> compareByCustomerId =(a,b)->{return a.getCustomerId().compareTo(b.getCustomerId());};
			Collections.sort(details, compareByCustomerId);
			JSONObject jsonOutput = buildJsonOutput(details);
			FileWriter file = new FileWriter(outputFilePath);
			 file.write(jsonOutput.toString());
	            file.flush();
			}
		 catch (Exception e) {
			// TODO: handle exception
			 System.out.println("THERE IS AN EXCEPTION HERE" +e );
		}
	}
	
	private Tap extractTap(JSONObject jsonTap) {
		Tap tap = new Tap();
		tap.setCustomerId((Long)jsonTap.get(JsonFields.CUSTOMER_ID.getProperty()));
		tap.setUnitTimeStamp((Long)jsonTap.get(JsonFields.TIME.getProperty()));
		tap.setStation((String)jsonTap.get(JsonFields.STATION.getProperty()));
		return tap;
	}
	
	private Integer calculateJourneyPrice(Tap start, Tap end) {
		ArrayList<String> zone1= new ArrayList<String>(Arrays.asList("A","B"));
		ArrayList<String> zone2= new ArrayList<String>(Arrays.asList("C","D","E"));
		ArrayList<String> zone3= new ArrayList<String>(Arrays.asList("C","E","F"));
		ArrayList<String> zone4= new ArrayList<String>(Arrays.asList("F","G","H","I"));
		Integer journeyCost = 0;
		if(zone1.contains(start.getStation())|| zone2.contains(start.getStation())) {
			if(zone1.contains(end.getStation())||zone2.contains(end.getStation())) {
				journeyCost= 240;
			}
			if("F".equals(end.getStation()) ) {
				journeyCost=280;
			}
			if(zone4.contains(end.getStation()) && !"F".equals(end.getStation())) {
				journeyCost=300;
				
			}
		}
		
		if(zone3.contains(start.getStation())) {
			if(zone3.contains(end.getStation())||zone4.contains(end.getStation())) {
				journeyCost= 200;
			}
			if("D".equals(end.getStation())||zone1.contains(end.getStation())) {
				journeyCost=280;
			}

		}
		
		if(zone4.contains(start.getStation())) {
			if(zone3.contains(end.getStation())||zone4.contains(end.getStation())) {
				journeyCost= 200;
			}
			if("D".equals(end.getStation())||zone1.contains(end.getStation())) {
				journeyCost=280;
			}

		}
		return journeyCost;
	}
	
	private Integer defineZoneStation(String station) {
		Integer zone = 0;
		switch (station) {
		case "A": 
		case "B": zone=1;break;
		case "C":
		case "D":
		case "E": zone=2;break;
		case "F": zone=3; break;
		case "G":
		case "H":
		case "I": zone=4;break;

		
		}
		
		
		return zone;
	}
	
	private JSONObject createOutputJsonObjectForOutput(TripDetails detail) {
		JSONObject output = new JSONObject();
		output.put(JsonFields.START_STATION.getProperty(), detail.getStationStart());
		output.put(JsonFields.END_STATION.getProperty(), detail.getStationEnd());
		output.put(JsonFields.JOURNEY_START_TIME.getProperty(), detail.getJourneyStart());
		output.put(JsonFields.COST_IN_CENTS.getProperty(), detail.getCost());
		output.put(JsonFields.START_ZONE.getProperty(), detail.getStartZone());
		output.put(JsonFields.END_ZONE.getProperty(), detail.getEndZone());
		return output;
	}
	
	private ArrayList<TripDetails> extractTripDetails(ArrayList<Tap> tapsList){
		ArrayList<TripDetails> details = new ArrayList<TripDetails>();
		
		
		
		for (int i = 0; i < tapsList.size(); i+=2) {
			//ordered list by taps and time
			
			Tap start = (Tap) tapsList.get(i);
			Tap end = (Tap) tapsList.get(i+1);
			TripDetails detail= new TripDetails();
			Integer journeyCost = calculateJourneyPrice(start, end);
			
			Integer startZone = defineZoneStation(start.getStation());
			Integer endZone= defineZoneStation(end.getStation());
			detail.setCost(journeyCost);
			detail.setCustomerId(start.getCustomerId());
			detail.setJourneyStart(start.getUnitTimeStamp());
			detail.setStartZone(startZone);
			detail.setEndZone(endZone);
			detail.setStationStart(start.getStation());
			detail.setStationEnd(end.getStation());
			details.add(detail);
		}
		return details;
	}
	
	private JSONObject buildJsonOutput(ArrayList<TripDetails> details) {
		JSONObject jsonOutput = new JSONObject();
		JSONArray outputTripsDetails = new JSONArray();
		JSONArray outputCustomerSummaries = new JSONArray();
		
		Integer totalTripsCost=0;
		Long customerId=0L;
		boolean firstElement= true;
		JSONObject trip = new JSONObject();
		JSONObject customerSummaries = new JSONObject();
		for(int i= 0;i<details.size();i++) {
			if(firstElement) {
				customerId = details.get(i).getCustomerId();
				totalTripsCost=details.get(i).getCost();
				trip = createOutputJsonObjectForOutput(details.get(i));
				outputTripsDetails.add(trip);
				firstElement = false;
				
				
			}
			else {
				if(details.get(i).getCustomerId().equals(details.get(i-1).getCustomerId()) ) {
					totalTripsCost+=details.get(i).getCost();
					trip = createOutputJsonObjectForOutput(details.get(i));
					outputTripsDetails.add(trip);
				}
				else {
					customerSummaries = new JSONObject();

					customerSummaries.put(JsonFields.CUSTOMER_ID.getProperty(), customerId);
					customerSummaries.put(JsonFields.TOTAL_COST_IN_CENTS.getProperty(), totalTripsCost);
					customerSummaries.put(JsonFields.TRIPS.getProperty(), outputTripsDetails);
					outputCustomerSummaries.add(customerSummaries);
					customerId = details.get(i).getCustomerId();
					totalTripsCost=details.get(i).getCost();
					outputTripsDetails= new JSONArray();
				}
			}
			if(i == details.size()-1) {
				jsonOutput.put(JsonFields.CUSTOMER_SUMMARY.getProperty(), outputCustomerSummaries);
			}
			
			
		}
		
		return jsonOutput;
	}

}
	
